import pandas as pd
import numpy
import sys

def ler_entrada_treino():
    try:
        dataset = pd.read_csv('clean_2011.csv', encoding='ISO-8859-1')
        return dataset
    except Exception as excecao:
        print("Ocorreu uma exceção ao ler o arquivo")
        print(f"Tipo da exceção: {type(excecao)}")
        print("Descrição da exceção:\n\n")
        print(excecao)
        sys.exit()