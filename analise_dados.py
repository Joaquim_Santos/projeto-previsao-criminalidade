import treinamento_teste
import limpeza_dados
import conexaoBD

dados_crimes = treinamento_teste.ler_entrada_treino()

dados_crimes = limpeza_dados.seleciona_limpa_atributos(dados_crimes)
conexaoBD.inserir_crimes(dados_crimes)