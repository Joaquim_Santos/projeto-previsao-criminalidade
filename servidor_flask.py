import json
import os
from flask import Flask, request
from flask_cors import CORS
from flask_pymongo import PyMongo
import conversor_gpx_json

app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb+srv://root:rootpassword@cluster0-uqnij.mongodb.net/tp1_ccf424?retryWrites=true&w=majority'
mongo = PyMongo(app)
# CORS enabled so react frontend can pull data from python backend
CORS(app)


@app.route('/inserir_rotas_mongodb')
def inserir_rotas_mongodb():
    contador_rota = 1
    rotas_ja_cadastradas = []
    collection = mongo.db.rotas
    arquivos_gpx = [arquivo for arquivo in os.listdir('../Rotas') if arquivo.endswith('.gpx')]
    for arquivo_gpx in arquivos_gpx:
        with open('../Rotas/' + arquivo_gpx, 'r') as dados_rota:
            rota = conversor_gpx_json.converter_gpx_json(dados_rota)
            rota['id_public_gps'] = int(arquivo_gpx.split('.gpx')[0])
            rota['nome'] = 'rota' + str(contador_rota)
            contador_rota += 1
            if(collection.find_one({"id_public_gps": rota['id_public_gps']}) == None):
                collection.insert_one(rota)
            else:
                rotas_ja_cadastradas.append(rota['id_public_gps'])
    return 'Sucesso na inserção / Rotas já cadastradas: %s' % (rotas_ja_cadastradas)


@app.route('/rota/<nome>')
def rota(nome):
    collection = mongo.db.rotas
    rota = collection.find_one({"nome": str(nome)})
    if(rota == None):
        return {"erro": "Rota \"%s\" não encontrada" % (nome)}
    else:
        del rota['_id']
        return rota


if __name__ == "__main__":
    app.run()
