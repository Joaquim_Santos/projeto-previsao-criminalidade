from pymongo import MongoClient

client = MongoClient("mongodb+srv://joaquim:projetodados2019@cluster0-mkb7e.mongodb.net/test?retryWrites=true&w=majority")
print(client.list_database_names())

banco_dados = client.criminalidade

def inserir_crimes(dados_crimes):
    chaves = list(dados_crimes.columns)
    dicionario_crimes = {}
    for chave in chaves:
        dicionario_crimes[chave] = dados_crimes[chave].tolist()
    
    for x in range(0, len(dicionario_crimes[chaves[0]])):
        try:
            banco_dados.crimes.insert_one({chaves[0]: dicionario_crimes[chaves[0]][x], chaves[1]: dicionario_crimes[chaves[1]][x], chaves[2]: dicionario_crimes[chaves[2]][x], 
            chaves[3]: dicionario_crimes[chaves[3]][x], chaves[4]: dicionario_crimes[chaves[4]][x], chaves[5]: dicionario_crimes[chaves[5]][x], 
            chaves[6]: dicionario_crimes[chaves[6]][x], chaves[7]: dicionario_crimes[chaves[7]][x], chaves[8]: dicionario_crimes[chaves[8]][x], 
            chaves[9]: dicionario_crimes[chaves[9]][x], chaves[10]: dicionario_crimes[chaves[10]][x], chaves[11]: dicionario_crimes[chaves[11]][x], 
            chaves[12]: dicionario_crimes[chaves[12]][x], chaves[13]: dicionario_crimes[chaves[13]][x], chaves[14]: dicionario_crimes[chaves[14]][x], 
            chaves[15]: dicionario_crimes[chaves[15]][x], chaves[16]: dicionario_crimes[chaves[16]][x]})
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a inserção ao Banco Mongo Atlas")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)
        
        #banco_dados.crimes.insert_one({"title": "My first post 3", "body": "This is the body of my first blog post 3", "slug": "first-post 3"})

    #for crime in client.criminalidade.crimes.find():
        #print(crime)
    