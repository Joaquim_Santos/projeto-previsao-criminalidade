import pandas as pd

def seleciona_limpa_atributos(conjunto_dados):
    atributos_considerados = ['DELEGACIA','ANO','MES', 'DATA_OCORRENCIA_BO', 'HORA_OCORRENCIA_BO', 'RUBRICA', 'FLAG_STATUS', 'CONDUTA', 'LATITUDE', 'LONGITUDE', 
    'CIDADE', 'LOGRADOURO', 'SEXO_PESSOA', 'IDADE_PESSOA','COR', 'DESCR_PROFISSAO', 'DESCR_GRAU_INSTRUCAO']
    
    #Eliminando boletins duolicados
    conjunto_dados['NUM_BO'] = conjunto_dados['NUM_BO'].apply(str)
    conjunto_dados['BO DELEGACIA'] = conjunto_dados['NUM_BO'] + conjunto_dados['DELEGACIA']
    conjunto_dados = conjunto_dados.drop_duplicates(subset='BO DELEGACIA')
    conjunto_dados = conjunto_dados.drop(['BO DELEGACIA'], axis=1)

    #Removendo datas nulas
    conjunto_dados.dropna(axis=0, subset=['DATA_OCORRENCIA_BO'], inplace=True)
    #Formatando datas
    datas = conjunto_dados['DATA_OCORRENCIA_BO'].tolist()
    datas_formatadas = []
    for data in datas:
        data = str(data).split('-')
        data = str(data[1]) + '/' + str(data[0]) + '/' + str(data[2])
        datas_formatadas.append(data)
    conjunto_dados['DATA_OCORRENCIA_BO'] = datas_formatadas
    
    #Mapeando horas em categorias
    horas_mapeadas =[]
    for dado in conjunto_dados['HORA_OCORRENCIA_BO']:
        try:
            dado = str(dado).split(':')[0]
            if int(dado) >= 0 and int(dado) < 6:
                dado = 'Madrugada'
            elif int(dado) >= 6 and int(dado) < 12:
                dado = 'Manhã'
            elif int(dado) >= 12 and int(dado) < 18:
                dado = 'Tarde'
            else:
                dado = 'Noite'
        except:
            dado = None
        
        horas_mapeadas.append(dado)

    conjunto_dados['HORA_OCORRENCIA_BO'] = horas_mapeadas
    #Substituindo horas nulas pelo período de maior frequência
    conjunto_dados['HORA_OCORRENCIA_BO'].fillna(value='Noite', inplace=True)
    
    #Removendo latitude e longitude nulas
    conjunto_dados.dropna(axis=0, subset=['LATITUDE'], inplace=True)
    conjunto_dados.dropna(axis=0, subset=['LONGITUDE'], inplace=True)

    #Removendo sexo nulo
    conjunto_dados.dropna(axis=0, subset=['SEXO_PESSOA'], inplace=True)

    #Mapeando idade em categorias
    idades_mapeadas =[]
    for dado in conjunto_dados['IDADE_PESSOA']:
        try:
            if int(dado) >= 0 and int(dado) < 13:
                dado = 'Criança'
            elif int(dado) >= 13 and int(dado) < 19:
                dado = 'Jovem'
            elif int(dado) >= 19 and int(dado) < 61:
                dado = 'Adulto'
            else:
                dado = 'Idoso'
        except:
            dado = None
        
        idades_mapeadas.append(dado)

    conjunto_dados['IDADE_PESSOA'] = idades_mapeadas
    #Ver o que fazer com idades nulas

    #Mapeando flag_status para um nome
    conjunto_dados['FLAG_STATUS'] = conjunto_dados['FLAG_STATUS'].map({'C' : 'Consumado', 'T' : 'Tentado'})
    
    #Removendo espaços a mais
    delegacias = conjunto_dados['DELEGACIA'].tolist()
    delegacias_formatadas = []
    for delegacia in delegacias:
        delegacia = str(delegacia).replace('  ', '')
        delegacias_formatadas.append(delegacia)
    conjunto_dados['DELEGACIA'] = delegacias_formatadas

    cidades = conjunto_dados['CIDADE'].tolist()
    cidades_formatadas = []
    for cidade in cidades:
        cidade = str(cidade).replace('  ', '')
        cidades_formatadas.append(cidade)
    conjunto_dados['CIDADE'] = cidades_formatadas

    logradouros = conjunto_dados['LOGRADOURO'].tolist()
    logradouros_formatados = []
    for logradouro in logradouros:
        logradouro = str(logradouro).replace('  ', '')
        logradouros_formatados.append(logradouro)
    conjunto_dados['LOGRADOURO'] = logradouros_formatados

    cores = conjunto_dados['COR'].tolist()
    cores_formatadas = []
    for cor in cores:
        cor = str(cor).replace('  ', '')
        cores_formatadas.append(cor)
    conjunto_dados['COR'] = cores_formatadas

    profissoes = conjunto_dados['DESCR_PROFISSAO'].tolist()
    profissoes_formatadas = []
    for profissao in profissoes:
        profissao = str(profissao).replace('  ', '')
        profissoes_formatadas.append(profissao)
    conjunto_dados['DESCR_PROFISSAO'] = profissoes_formatadas

    escolaridades = conjunto_dados['DESCR_GRAU_INSTRUCAO'].tolist()
    escolaridades_formatadas = []
    for escolaridade in escolaridades:
        escolaridade = str(escolaridade).replace('  ', '')
        escolaridades_formatadas.append(escolaridade)
    conjunto_dados['DESCR_GRAU_INSTRUCAO'] = escolaridades_formatadas

    return conjunto_dados[atributos_considerados]